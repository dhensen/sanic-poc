# Medium blogpost

about do/don'ts or good/bad practices when performing a loadtest.

hardware: CPU: Intel Core i5-3570K @ 4x 3.8GHz

```
$ wrk
Usage: wrk <options> <url>
  Options:
    -c, --connections <N>  Connections to keep open
    -d, --duration    <T>  Duration of test
    -t, --threads     <N>  Number of threads to use

    -s, --script      <S>  Load Lua script file
    -H, --header      <H>  Add header to request
        --latency          Print latency statistics
        --timeout     <T>  Socket/request timeout
    -v, --version          Print version details

  Numeric arguments may include a SI unit (1k, 1M, 1G)
  Time arguments may include a time unit (2s, 2m, 2h)
```

When starting the sanic app via vscode commandline:
```
$ wrk -t50 -c100 http://localhost:8000
Running 10s test @ http://localhost:8000
  50 threads and 100 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency    10.01ms    9.29ms  76.15ms   72.74%
    Req/Sec   238.25    246.97     2.80k    93.65%
  118866 requests in 10.08s, 13.60MB read
Requests/sec:  11789.90
Transfer/sec:      1.35MB
```

When starting the sanic app via a "normal"/non-integrated terminal (termite was used here):
```
$ wrk -t50 -c100 http://localhost:8000
Running 10s test @ http://localhost:8000
  50 threads and 100 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     5.39ms    3.35ms  33.85ms   76.28%
    Req/Sec   392.17    182.50     3.43k    74.55%
  196087 requests in 10.09s, 22.44MB read
Requests/sec:  19428.79
Transfer/sec:      2.22MB
```

When starting the sanic app via a non-integrated terminal and also disable debug and access_logs:

```
$ wrk -t50 -c100 http://localhost:8000
Running 10s test @ http://localhost:8000
  50 threads and 100 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     1.85ms  597.19us  17.20ms   92.24%
    Req/Sec     1.10k   195.94     4.62k    78.32%
  547164 requests in 10.10s, 62.62MB read
Requests/sec:  54180.60
Transfer/sec:      6.20MB
```


## Conclusion

When you want to perform a loadtest, make sure the environment does not slow it down more than on a production environment.

Ofcourse disabling logs is probably a no-go in production for most businesses that need auditability, but this just shows how much logging could weigh on your performance.

> Disclaimer: the http handler function is not doing much, so logging is going to outweigh this really fast in this example.
