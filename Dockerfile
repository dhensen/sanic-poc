FROM python:3.8.2-alpine3.11
RUN apk --no-cache add build-base linux-headers
RUN pip install pipenv

RUN mkdir -p /app
WORKDIR /app

COPY Pipfile /app/
RUN pipenv install --system --skip-lock

COPY api.py /app/

EXPOSE 8000
CMD ["python3", "api.py"]
