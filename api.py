from sanic import Sanic
from sanic.exceptions import abort
from sanic.response import text, json
from sanic_prometheus import monitor
import random

from prometheus_client import Counter
exception_counter = Counter('exception_count', 'exception count')

app = Sanic(name='poc')


@app.route('/')
async def get_ip(request):
    if random.random() < 0.1:
        raise Exception('kaboem')

    ip = request.headers.get('x-real-ip') or request.ip
    return json({'data': {'ip': ip}})


@app.exception(Exception)
async def all_exceptions(request, exception):
    exception_counter.inc()
    return json({'error': {'message': 'server_error'}}, status=500)


if __name__ == "__main__":
    import os
    extra = {}
    if os.getenv('SKIP_LOGGING'):
        print('SKIP_LOGGING env var is set:')
        print(' - disabling debug')
        extra['debug'] = False
        print(' - disabling access_log')
        extra['access_log'] = False

    # option 1 - add /metrics endpoint next to other endpoints
    monitor(app).expose_endpoint()
    extra['workers'] = 4

    # option 2 - run /metrics on a separate port
    # monitor(app).start_server(addr="0.0.0.0", port=8001)
    # this one does not work with multiple workers

    app.run(host="0.0.0.0", port=8000, **extra)
