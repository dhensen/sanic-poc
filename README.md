# How much RPS does Sanic give me out of the box?

## Usage

```
pipenv install
pipenv shell
python3 ./api.py
```

The api is now running locally on port 8000.

## Run a benchmark

> On Arch-Linux `wrk` is available in the AUR. `yay -S wrk`

To run a 10 second benchmark with 50 threads and 100 connections:

```
wrk -t50 -c100 http://localhost:8000
```
