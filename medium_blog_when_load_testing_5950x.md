# Medium blogpost

about do/don'ts or good/bad practices when performing a loadtest.

hardware: CPU: AMD 5950X

```
$ wrk
Usage: wrk <options> <url>
  Options:
    -c, --connections <N>  Connections to keep open
    -d, --duration    <T>  Duration of test
    -t, --threads     <N>  Number of threads to use

    -s, --script      <S>  Load Lua script file
    -H, --header      <H>  Add header to request
        --latency          Print latency statistics
        --timeout     <T>  Socket/request timeout
    -v, --version          Print version details

  Numeric arguments may include a SI unit (1k, 1M, 1G)
  Time arguments may include a time unit (2s, 2m, 2h)
```

When starting the sanic app via vscode commandline:
```
$ wrk -t50 -c100 http://localhost:8000
Running 10s test @ http://localhost:8000
  50 threads and 100 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency    27.75ms   18.63ms 146.18ms   66.39%
    Req/Sec    74.94     47.85   620.00     86.79%
  37724 requests in 10.10s, 4.32MB read
Requests/sec:   3735.87
Transfer/sec:    437.80KB
```

When starting the sanic app via a "normal"/non-integrated terminal (termite was used here):
```
$ wrk -t50 -c100 http://localhost:8000
Running 10s test @ http://localhost:8000
  50 threads and 100 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     2.69ms    2.93ms  21.53ms   87.07%
    Req/Sec     1.05k   426.48     2.87k    75.30%
  526556 requests in 10.08s, 60.26MB read
Requests/sec:  52256.81
Transfer/sec:      5.98MB`
```

When starting the sanic app via a non-integrated terminal and also disable debug and access_logs:

```
$ wrk -t50 -c100 http://localhost:8000
Running 10s test @ http://localhost:8000
  50 threads and 100 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency   566.10us  132.38us   2.67ms   93.24%
    Req/Sec     3.55k   471.86     5.22k    66.07%
  1782782 requests in 10.10s, 204.02MB read
Requests/sec: 176527.96
Transfer/sec:     20.20MB
```


## Conclusion

When you want to perform a loadtest, make sure the environment does not slow it down more than on a production environment.

Ofcourse disabling logs is probably a no-go in production for most businesses that need auditability, but this just shows how much logging could weigh on your performance.

> Disclaimer: the http handler function is not doing much, so logging is going to outweigh this really fast in this example.
